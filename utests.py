#!/usr/bin/env python

import sys
import unittest as ut
import logging
import numpy as np
from computational_stopwatch import Stopwatch
from ba_lorre.lorre import LORRE
import benchmark_functions as bf

N_RUNS 							= 100
MAX_ITERATIONS					= 1000
SCORE_THRESHOLD 				= 0.001
ITERATIONS_MEAN_THRESHOLD 		= 1.4
TEST_STD_DEV 					= False # removing all the std_dev tests since they can be wildly variable
ITERATIONS_STD_DEV_THRESHOLD 	= 1.7
EXPECTED_PARALLEL_GAIN			= 0.9

# Griewank_lorre_parameters=	{'nb':5,	'nrb':30,	'stlim':15, 'derating_type': 'linear'}
Ackley_lorre_parameters=		{'nb':5,	'nrb':30,	'stlim':15, 'derating_type': 'linear'}
# Easom_bees_parameters=		{'ns':0,	'nb':14,	'ne':1,	'nrb':5,	'nre':30,	'stlim':10, 'useSimplifiedParameters':True}
Schwefel_lorre_parameters =	{'nb':5,	'nrb':30,	'stlim':15, 'derating_type': 'linear'}

functions_list = [
	{'function': bf.Schwefel(n_dimensions=2, opposite=True), 'esp_iter_mean': 44.58 ,'esp_iter_std_dev': 13.64, 'iter_q3_limit': 50, 'parameters': Schwefel_lorre_parameters},
	# {'function': bf.Easom(opposite=True), 'esp_iter_mean': 38.28 ,'esp_iter_std_dev': 4.94, 'parameters': Easom_bees_parameters},
	{'function': bf.Ackley(n_dimensions=10, opposite=True), 'esp_iter_mean': 128.82 ,'esp_iter_std_dev': 29.77, 'iter_q3_limit': 130, 'parameters': Ackley_lorre_parameters},
	# {'function': bf.Griewank(n_dimensions=10, opposite=True), 'esp_iter_mean': 2659.06 ,'esp_iter_std_dev': 1889.61, 'iter_q3_limit': 100, 'parameters': Griewank_lorre_parameters},
]

functions_running_time = {}

def test_algorithm(function_to_test, lorre_parameters):
	global functions_running_time
	results = []
	s = Stopwatch()
	for i in range(N_RUNS):
		lb, ub = function_to_test.suggested_bounds()
		alg = LORRE(function_to_test, range_min=lb, range_max=ub, **lorre_parameters)
		with Stopwatch(f"Run {i}"):
			it, score = alg.performFullOptimisation(max_iteration=MAX_ITERATIONS, max_score=function_to_test.maximum().score - SCORE_THRESHOLD)
		results+=[it]
		print('iteration', it, 'score', score, 'maxium', function_to_test.maximum(), 'minimum', function_to_test.minimum())
	if function_to_test.name() not in functions_running_time:
		functions_running_time[function_to_test.name()] = s.get_elapsed_time()
	return np.array(results)


class TestOnBenchmarkFunctions(ut.TestCase):
	def test_functions(self):
		for function in functions_list:
			test_function = function['function']
			with Stopwatch(f"Benchmark function {test_function.name()}"):
				results = test_algorithm(test_function, function['parameters'])
				results_5ns = np.percentile(results, [0, 25, 50, 75, 100])
				print(results_5ns)
				if results_5ns[-2]>function['iter_q3_limit']:
					self.fail(f"Testing function {test_function.name()} q3 number of iterations greater than the limit {results_5ns[-2]}>{function['iter_q3_limit']}")

if __name__=='__main__':
	logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
	ut.main()