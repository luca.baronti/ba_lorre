# Versions History

## v1.0.3
- worst solutions in a plateau are selected by proximity to the optimum solution

## v1.0.2
- minor fix with a parameters hint

## v1.0.1
- minor visual fixes for pypi

## v1.0.0
- first release
